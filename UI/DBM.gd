extends Node

onready var StarParticles = get_node("/root/DBM/StarParticles")

func _ready():
	get_node("MarginContainer/HSplitContainer/Interface").connect("blink_stars", self, "emit_stars")

func emit_stars():
	var new_particle_pos = get_viewport().get_mouse_position()
	StarParticles.position = new_particle_pos
	StarParticles.restart()
