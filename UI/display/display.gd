extends Control


### BIRDS RES
const pigeon_controler:PackedScene = preload("res://UI/display/birds/pigeon_controler.tscn")
const duck_controler:PackedScene = preload("res://UI/display/birds/duck_controler.tscn")
const chicken_controler:PackedScene = preload("res://UI/display/birds/chicken_controler.tscn")


### OUTFITS RES
const pigeon_chasuble_controler:PackedScene = preload("res://UI/display/outfits/pigeon/pigeon_chasuble_controler.tscn")
const pigeon_giletjaune_controler:PackedScene = preload("res://UI/display/outfits/pigeon/pigeon_giletjaune_controler.tscn")
const pigeon_queuedepie_controler:PackedScene = preload("res://UI/display/outfits/pigeon/pigeon_queuedepie_controler.tscn")
const pigeon_dress_controler:PackedScene = preload("res://UI/display/outfits/pigeon/pigeon_dress_controler.tscn")

const duck_chasuble_controler:PackedScene = preload("res://UI/display/outfits/duck/duck_chasuble_controler.tscn")
const duck_giletjaune_controler:PackedScene = preload("res://UI/display/outfits/duck/duck_giletjaune_controler.tscn")
const duck_queuedepie_controler:PackedScene = preload("res://UI/display/outfits/duck/duck_queuedepie_controler.tscn")
const duck_dress_controler:PackedScene = preload("res://UI/display/outfits/duck/duck_dress_controler.tscn")

const chicken_chasuble_controler:PackedScene = preload("res://UI/display/outfits/chicken/chicken_chasuble_controler.tscn")
const chicken_giletjaune_controler:PackedScene = preload("res://UI/display/outfits/chicken/chicken_giletjaune_controler.tscn")
const chicken_queuedepie_controler:PackedScene = preload("res://UI/display/outfits/chicken/chicken_queuedepie_controler.tscn")
const chicken_dress_controler:PackedScene = preload("res://UI/display/outfits/chicken/chicken_dress_controler.tscn")


### HATS RES
const pigeon_helmet_controler:PackedScene = preload("res://UI/display/hats/pigeon/pigeon_helmet_controler.tscn")
const pigeon_hautdeforme_controler:PackedScene = preload("res://UI/display/hats/pigeon/pigeon_hautdeforme_controler.tscn")
const pigeon_chapeau_controler:PackedScene = preload("res://UI/display/hats/pigeon/pigeon_chapeau_controler.tscn")

const duck_helmet_controler:PackedScene = preload("res://UI/display/hats/duck/duck_helmet_controler.tscn")
const duck_hautdeforme_controler:PackedScene = preload("res://UI/display/hats/duck/duck_hautdeforme_controler.tscn")
const duck_chapeau_controler:PackedScene = preload("res://UI/display/hats/duck/duck_chapeau_controler.tscn")

const chicken_helmet_controler:PackedScene = preload("res://UI/display/hats/chicken/chicken_helmet_controler.tscn")
const chicken_hautdeforme_controler:PackedScene = preload("res://UI/display/hats/chicken/chicken_hautdeforme_controler.tscn")
const chicken_chapeau_controler:PackedScene = preload("res://UI/display/hats/chicken/chicken_chapeau_controler.tscn")


### EYES RES
const pigeon_eye_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_eye_controler.tscn")
const pigeon_angry_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_angry_controler.tscn")
const pigeon_anxious_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_anxious_controler.tscn")
const pigeon_lash_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_lash_controler.tscn")
const pigeon_lash2_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_lash2_controler.tscn")
const pigeon_dead_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_dead_controler.tscn")
const pigeon_sad_controler:PackedScene = preload("res://UI/display/eyes/pigeon/pigeon_sad_controler.tscn")

const duck_eye_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_eye_controler.tscn")
const duck_angry_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_angry_controler.tscn")
const duck_anxious_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_anxious_controler.tscn")
const duck_lash_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_lash_controler.tscn")
const duck_lash2_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_lash2_controler.tscn")
const duck_dead_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_dead_controler.tscn")
const duck_sad_controler:PackedScene = preload("res://UI/display/eyes/duck/duck_sad_controler.tscn")

const chicken_eye_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_eye_controler.tscn")
const chicken_angry_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_angry_controler.tscn")
const chicken_anxious_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_anxious_controler.tscn")
const chicken_lash_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_lash_controler.tscn")
const chicken_lash2_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_lash2_controler.tscn")
const chicken_dead_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_dead_controler.tscn")
const chicken_sad_controler:PackedScene = preload("res://UI/display/eyes/chicken/chicken_sad_controler.tscn")


### NODE
onready var interface_node:Node = get_node("/root/DBM/MarginContainer/HSplitContainer/Interface")

onready var bird_controler:Node = get_node("MarginContainer/CenterContainer/BirdControler")
onready var outfit_controler:Node = get_node("MarginContainer/CenterContainer/OutfitControler")
onready var eye_controler:Node = get_node("MarginContainer/CenterContainer/EyeControler")
onready var hat_controler:Node = get_node("MarginContainer/CenterContainer/HatControler")

onready var star_controler = get_node("MarginContainer/CenterContainer/StarParticles")


### VARS
var current_bird:int = 0
var current_outfit:int = 0
var current_hat:int = 0
var current_eye:int = 0


# Connect the signals
func _ready():
	var signal_connection_err = 0
	signal_connection_err += interface_node.connect("change_bird", self, "display_bird")
	signal_connection_err += interface_node.connect("change_outfit", self, "display_outfit")
	signal_connection_err += interface_node.connect("change_eye", self, "display_eye")
	signal_connection_err += interface_node.connect("change_hat", self, "display_hat")
	
	if signal_connection_err:
		print("Signal connect error in display.gd")
		get_tree().quit()


func display_stars():
	var new_stars_pos:Vector2 = get_node("Background").get_size()/2
	new_stars_pos.x -= 60
	new_stars_pos.y -= 100
	star_controler.position = new_stars_pos
	star_controler.restart()


# Change the bird displayed and update the depending stuff
func display_bird(bird_id:int) -> void:
	for child in bird_controler.get_children():
		child.queue_free()
	current_bird = bird_id
	display_outfit(current_outfit)
	display_hat(current_hat)
	display_eye(current_eye)
	
	display_stars()
	
	match bird_id:
		0:
			bird_controler.add_child(pigeon_controler.instance())
		1:
			bird_controler.add_child(duck_controler.instance())
		2:
			bird_controler.add_child(chicken_controler.instance())


# Change the outfit displayed
func display_outfit(outfit_id:int) -> void:
	for child in outfit_controler.get_children():
		child.queue_free()
	current_outfit = outfit_id
	
	display_stars()
	
	match outfit_id:
		0:
			pass
		1:
			match current_bird:
				0:
					outfit_controler.add_child(pigeon_chasuble_controler.instance())
				1:
					outfit_controler.add_child(duck_chasuble_controler.instance())
				2:
					outfit_controler.add_child(chicken_chasuble_controler.instance())
		2:
			match current_bird:
				0:
					outfit_controler.add_child(pigeon_giletjaune_controler.instance())
				1:
					outfit_controler.add_child(duck_giletjaune_controler.instance())
				2:
					outfit_controler.add_child(chicken_giletjaune_controler.instance())
		3:
			match current_bird:
				0:
					outfit_controler.add_child(pigeon_queuedepie_controler.instance())
				1:
					outfit_controler.add_child(duck_queuedepie_controler.instance())
				2:
					outfit_controler.add_child(chicken_queuedepie_controler.instance())
		4:
			match current_bird:
				0:
					outfit_controler.add_child(pigeon_dress_controler.instance())
				1:
					outfit_controler.add_child(duck_dress_controler.instance())
				2:
					outfit_controler.add_child(chicken_dress_controler.instance())


func display_hat(hat_id:int) -> void:
	for child in hat_controler.get_children():
		child.queue_free()
	current_hat = hat_id
	
	display_stars()
	
	match hat_id:
		0:
			pass
		1:
			match current_bird:
				0:
					hat_controler.add_child(pigeon_helmet_controler.instance())
				1:
					hat_controler.add_child(duck_helmet_controler.instance())
				2:
					hat_controler.add_child(chicken_helmet_controler.instance())
		2:
			match current_bird:
				0:
					hat_controler.add_child(pigeon_hautdeforme_controler.instance())
				1:
					hat_controler.add_child(duck_hautdeforme_controler.instance())
				2:
					hat_controler.add_child(chicken_hautdeforme_controler.instance())
		3:
			match current_bird:
				0:
					hat_controler.add_child(pigeon_chapeau_controler.instance())
				1:
					hat_controler.add_child(duck_chapeau_controler.instance())
				2:
					hat_controler.add_child(chicken_chapeau_controler.instance())


func display_eye(eye_id:int) -> void:
	for child in eye_controler.get_children():
		child.queue_free()
	current_eye = eye_id
	
	display_stars()
	
	match eye_id:
		# Basic bird eyes
		0:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_eye_controler.instance())
				1:
					eye_controler.add_child(duck_eye_controler.instance())
				2:
					eye_controler.add_child(chicken_eye_controler.instance())
		1:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_angry_controler.instance())
				1:
					eye_controler.add_child(duck_angry_controler.instance())
				2:
					eye_controler.add_child(chicken_angry_controler.instance())
		2:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_anxious_controler.instance())
				1:
					eye_controler.add_child(duck_anxious_controler.instance())
				2:
					eye_controler.add_child(chicken_anxious_controler.instance())
		3:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_lash_controler.instance())
				1:
					eye_controler.add_child(duck_lash_controler.instance())
				2:
					eye_controler.add_child(chicken_lash_controler.instance())
		4:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_lash2_controler.instance())
				1:
					eye_controler.add_child(duck_lash2_controler.instance())
				2:
					eye_controler.add_child(chicken_lash2_controler.instance())
		5:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_dead_controler.instance())
				1:
					eye_controler.add_child(duck_dead_controler.instance())
				2:
					eye_controler.add_child(chicken_dead_controler.instance())
		6:
			match current_bird:
				0:
					eye_controler.add_child(pigeon_sad_controler.instance())
				1:
					eye_controler.add_child(duck_sad_controler.instance())
				2:
					eye_controler.add_child(chicken_sad_controler.instance())
#
