extends TabContainer

var bird_icon:Texture = preload("res://images/tabs_icons/bird.png")
var outfit_icon:Texture = preload("res://images/tabs_icons/outfit.png")
var hat_icon:Texture = preload("res://images/tabs_icons/hat.png")
var eye_icon:Texture = preload("res://images/tabs_icons/eye.png")


signal change_bird(bird_id)
signal change_outfit(outfit_id)
signal change_hat(hat_id)
signal change_eye(eye_id)
signal change_prop(prop_id)
signal change_expression(expression_id)

signal blink_stars


func _ready():
	for i in range(get_tab_count()):
		set_tab_title(i,"")
	set_tab_icon(0, bird_icon)
	set_tab_icon(1, outfit_icon)
	set_tab_icon(2, hat_icon)
	set_tab_icon(3, eye_icon)
	
	get_node("Bird/Pigeon").set_pressed(true)
	get_node("Outfit/NoOutfit").set_pressed(true)
	get_node("Hat/NoHat").set_pressed(true)
	get_node("Eye/Vanilla").set_pressed(true)


### BIRDS

func _on_Pigeon_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_bird", 0)

func _on_Duck_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_bird", 1)

func _on_Chicken_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_bird", 2)


### OUTFITS

func _on_NoOutfit_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_outfit", 0)

func _on_Chasuble_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_outfit", 1)

func _on_GiletJaune_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_outfit", 2)

func _on_QueueDePie_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_outfit", 3)

func _on_Dress_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_outfit", 4)


### HATS

func _on_NoHat_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_hat", 0)

func _on_Helmet_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_hat", 1)

func _on_HautDeForme_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_hat", 2)

func _on_Chapeau_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_hat", 3)


### EYES

func _on_Vanilla_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 0)

func _on_Angry_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 1)


func _on_Anxious_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 2)


func _on_Lash_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 3)


func _on_Lash2_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 4)


func _on_Dead_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 5)


func _on_Sad_toggled(button_pressed):
	if button_pressed:
		emit_signal("change_eye", 6)


### COOL STUFF
func _on_Interface_tab_changed(tab):
	emit_signal("blink_stars")
